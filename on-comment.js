var entities = require('@jetbrains/youtrack-scripting-api/entities');
var http = require('@jetbrains/youtrack-scripting-api/http');
var workflow = require('@jetbrains/youtrack-scripting-api/workflow');
var common = require('./common');

exports.rule = entities.Issue.onChange({
  title: workflow.i18n('Send notification to discord on new issue comments'),
  guard: function(ctx) {
    return ctx.issue.comments.added.isNotEmpty();
  },
  action: function(ctx) {
    var issue = ctx.issue;

    var issueLink = '<' + issue.url + "|" + issue.id + '>';

    issue.comments.added.forEach(function(comment) {
      var commentLink = '<' + comment.url + "|" + issue.summary + '>';

      var author = comment.author.fullName;

      var payload = {
        "attachments": [{
          "fallback": commentLink + " (" + issueLink + ")",
          "pretext": commentLink + " (" + issueLink + ")",
          "color": "#edb431",
          "author_name": author,
          "fields": [{
            "title": "Message",
            value: common.replaceMentions(comment.text)
          }]
        }]
      };

      var mentions = common.getAllMentions(comment.text);
      if (mentions.length > 0) {
        payload.text = mentions.join(' ');
      }

      var connection = new http.Connection(common.WEBHOOK_URL, null, 2000);
      var response = connection.postSync('', null, JSON.stringify(payload));
      if (!response.isSuccess) {
        console.warn('Failed to post notification to Slack. Details: ' + response.toString());
      }

    });

  },
  requirements: {}
});