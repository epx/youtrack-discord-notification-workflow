/**
 * This is a template for a custom script. Here you can add any
 * functions and objects you want to. To use these functions and objects in other
 * scripts, add them to the 'exports' object.
 * Use the following syntax to reference this script:
 *
 * `var myScript = require('./this-script-name');`
 *
 * For details, read the Quick Start Guide:
 * https://www.jetbrains.com/help/youtrack/incloud/2021.1/Quick-Start-Guide-Workflows-JS.html
 */

// IMPORTANT: Use a valid Incoming Webhook from Slack. To get one, go to https://my.slack.com/services/new/incoming-webhook/

const WEBHOOK_URL = 'https://discordapp.com/api/webhooks/578874849834041354/vBXZOdTwSg7Zju9am1vRPP9-EZrUjYiXO9EKwQRBWwdTQg94p7hLNHqEKbAIcFvShnF6/slack';
const COLORS = {
  onNew: '#000080',
  onStateChange: '#408002',
  onResolve: '#108040',
  onReopen: '#800002'
};
const PEOPLE = {
  'jgonzalez': '153932217591005184',
  'b.branchard': '804050440823898202',
  'rleclercq': '214463773119873024'
};
const DEV_ROLE_MENTION = '<@&438623518629036054>';
const getMention = (login) => {
  if (PEOPLE[login]) {
    return "<@" + PEOPLE[login] + ">";
  }
  return login + " (discord id not set in `common`->`PEOPLE`)";
};

const getAllMentions = (text) => {
  var mentions = [];
  const regex = /@([\w.]+)/gm;

  let m;
  while ((m = regex.exec(text)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }
    // The result can be accessed through the `m`-variable.
    m.forEach((match, groupIndex) => {
      if (groupIndex === 1) {
        mentions.push(getMention(match));
      }
    });
  }

  return mentions;
};

const replaceMentions = (text) => {
  console.log(text);
  if (!text) {
    return text;
  }
  text = " " + text; // force as string

  var mentions = [];
  for (let login in PEOPLE) {
    while (text.includes("@" + login)) {
      text = text.replace("@" + login, getMention(login));
    }
  }
  return text;
};

exports.WEBHOOK_URL = WEBHOOK_URL;
exports.COLORS = COLORS;
exports.PEOPLE = PEOPLE;
exports.DEV_ROLE_MENTION = DEV_ROLE_MENTION;
exports.getMention = getMention;
exports.getAllMentions = getAllMentions;
exports.replaceMentions = replaceMentions;
exports.isHotfixIssue = (issue) => {
  return issue.fields.Priority.name.includes("Hotfix");
};