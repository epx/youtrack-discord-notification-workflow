// Thanks to Michael Rush for the original version of this rule (https://software-development.dfstudio.com/youtracks-new-javascript-workflows-make-slack-integration-a-breeze-d3275605d565)

var entities = require('@jetbrains/youtrack-scripting-api/entities');
var http = require('@jetbrains/youtrack-scripting-api/http');
var workflow = require('@jetbrains/youtrack-scripting-api/workflow');
var common = require('./common');

exports.rule = entities.Issue.onChange({
  title: workflow.i18n('Send notification to discord when an issue is reported, resolved, or reopened'),
  guard: function(ctx) {
    return ctx.issue.becomesReported ||
      ctx.issue.fields.becomesResolved ||
      ctx.issue.becomesUnresolved ||
      (ctx.issue.isReported && ctx.issue.fields.isChanged(ctx.State) || ctx.issue.fields.isChanged(ctx.Assignee));
  },
  action: function(ctx) {
    var issue = ctx.issue;

    var pretext;
    var text;
    var color;

    var isNew = issue.becomesReported;

    var issueLink = '<' + issue.url + "|" + issue.id + '>';
    var state = issue.fields.State && issue.fields.State.name;
    if (!isNew && issue.fields.isChanged(ctx.State) && issue.oldValue(ctx.State)) {
      state = "~~" + issue.oldValue(ctx.State).name + "~~ " + state;
    }

    if (isNew) {
      pretext = "Created";
      text = issue.descripton;
      color = common.COLORS.onNew;
    } else if (issue.becomesResolved) {
      pretext = "Resolved";
      color = common.COLORS.onResolve;
    } else if (issue.becomesUnresolved) {
      pretext = common.COLORS.onReopen;
    } else if (issue.fields.isChanged(ctx.State)) {
      color = common.COLORS.onStateChange;
      pretext = "State changed to " + issue.fields.State.name;
    }
    if (issue.fields.isChanged(ctx.Assignee)) {
      if (pretext) {
        pretext += "\n";
      }
      pretext = "Assignee changed to " + issue.fields.Assignee.fullName;
    }

    var changedByName = isNew ? issue.reporter.fullName : issue.updatedBy.fullName;

    if (issue.id == "Issue.Draft" || issue.summary == null) {
      return;
    }

    var text_content = '';
    if (common.isHotfixIssue(issue)) {
      text_content = "/!\\ Hotfix ";
      if (!issue.fields.Assignee) {
        text_content += " " + common.DEV_ROLE_MENTION;
      } else {
        text_content += " " + common.getMention(issue.fields.Assignee.login);
      }
    }

    var payload = {
      "text": text_content,
      "attachments": [{
        "fallback": pretext + " " + issue.summary + " (" + issueLink + ")",
        "pretext": pretext,
        "color": color,
        "author_name": changedByName,
        "title": issue.id + " " + issue.summary,
        "title_link": issue.url,
        "text": common.replaceMentions(text),
        "fields": [{
            "title": "State",
            "value": state,
            "short": true
          },
          {
            "title": "Priority",
            "value": issue.fields.Priority ? issue.fields.Priority.name : "Empty",
            "short": true
          },
          {
            "title": "Assignee",
            "value": issue.fields.Assignee ? issue.fields.Assignee.fullName : "Empty",
            "short": true
          }
        ]
      }]
    };

    var connection = new http.Connection(common.WEBHOOK_URL, null, 2000);
    var response = connection.postSync('', null, JSON.stringify(payload));
    if (!response.isSuccess) {
      console.warn('Failed to post notification to Slack. Details: ' + response.toString());
    }
  },
  requirements: {
    Priority: {
      type: entities.EnumField.fieldType
    },
    State: {
      type: entities.State.fieldType
    },
    Assignee: {
      type: entities.User.fieldType
    }
  }
});
